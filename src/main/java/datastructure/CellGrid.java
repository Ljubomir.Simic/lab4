package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int columns;
    private CellState initialState;
    private CellState[][] cells;

    public CellGrid(int rows, int columns, CellState initialState) {
        // TODO Auto-generated constructor stub
        this.rows = rows;
        this.columns = columns;
        this.initialState = initialState;

        cells = new CellState[rows][columns];

        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numColumns(); col++) {
                cells[row][col] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return cells.length;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cells[0].length;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        try {
            cells[row][column] = element;
        } catch (IllegalArgumentException e) {
            System.out.println(e);
        }

    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        try {
            return cells[row][column];
        } catch (IllegalArgumentException e) {
            System.out.println(e);
            return null;
        }
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid copy = new CellGrid(numRows(), numColumns(), initialState);
        for (int row = 0; row < numRows(); row++) {
            for (int col = 0; col < numColumns(); col++) {
                copy.set(row, col, cells[row][col]);
            }
        }
        return copy;
    }
}
