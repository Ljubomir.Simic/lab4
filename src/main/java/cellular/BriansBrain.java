package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int row = 0; row < numberOfRows(); row++) {
            for (int col = 0; col < numberOfColumns(); col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }
        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState currentCellState = getCellState(row, col);
        int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);

        if (currentCellState == CellState.ALIVE) { return CellState.DYING; }
        else if (currentCellState == CellState.DYING) { return CellState.DEAD; }
        else if (currentCellState == CellState.DEAD && aliveNeighbors == 2) { return CellState.ALIVE; }
        return CellState.DEAD;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    private int countNeighbors(int row, int col, CellState state) {
        // TODO
        int count = 0;
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if (currentGeneration.get(row + i, col + j) == state) {
                    if (i == 0 && j == 0) { // Counts every neighbouring cell except itself...
                        continue;
                    }
                    count++;
                }
            }
        }
        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
